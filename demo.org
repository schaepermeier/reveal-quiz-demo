# Local IspellDict: en
#+STARTUP: showeverything
#+INCLUDE: config.org

#+TITLE: reveal.js-quiz demo
#+AUTHOR: Lennart Schäpermeier
#+DATE: June 2018

* Introduction

** Dynamic Quizzes for reveal.js

- The [[https://gitlab.com/schaepermeier/reveal.js-quiz][reveal.js-quiz plugin]] lets you create dynamic quizzes in your [[https://github.com/hakimel/reveal.js/][reveal.js]] presentation
- Quizzes do not require an internet connection and can be re-taken arbitrarily often
- Simple text-editor suffices to define quizzes
- Suited for self-study materials based on reveal.js
- Based on the jQuery-Plugin [[https://github.com/jewlofthelotus/SlickQuiz][SlickQuiz]] by Julie Cameron

** A Sample Quiz

#+REVEAL_HTML: <script data-quiz src="./quizzes/dummy-quiz.js"></script>

* Usage

** Configuration for reveal.js

1. Download the plugin from its [[https://gitlab.com/schaepermeier/reveal.js-quiz][git repository]]
2. Copy it to the plugins folder of your reveal.js configuration
3. Load it as a dependency in Reveal.initialize, as seen below
4. Define a quiz-variable within a ~<script data-quiz>~-Tag (see next slide)

#+BEGIN_SRC js
Reveal.initialize({
  dependencies: [
    // other dependencies
    { src: 'plugin/quiz/js/quiz.js', async: true, callback: function() { prepareQuizzes(); } }
    // other dependencies
  ]
});
#+END_SRC

** Usage in reveal.js

#+BEGIN_SRC html
<script data-quiz>
quiz = {
  "info": { /* some general information about the quiz */ },
  "questions": [
    { // Question 1 - Multiple Choice, Single True Answer
      "q": "What number is the letter A in the English alphabet?",
      "a": [
        {"option": "14",     "correct": false},
        {"option": "1",      "correct": true}
        // more options
      ],
      "correct": "<p><span>That's right!</span> The letter A is the first letter in the alphabet!</p>",
      "incorrect": "<p><span>Uhh no.</span> It's the first letter of the alphabet. Did you actually <em>go</em> to kindergarden?</p>"
    },
    /* more questions */
  ]
}
</script>
#+END_SRC

** Quiz options

- Quizzes can be single- or multiple-choice (select all and select any)
- Responses to correct and wrong answer can be defined per question
- Ranking at the end of the quiz allows for overall feedback
- Most configuration options can be seen in the source code [[https://gitlab.com/schaepermeier/reveal-quiz-demo/blob/master/quizzes/dummy-quiz.js][here]] and all are described in the  [[https://gitlab.com/schaepermeier/reveal.js-quiz][plugin documentation]]

** Usage with emacs-reveal or org-reveal

- As a matter of fact, this presentation was generated with [[https://gitlab.com/oer/emacs-reveal][emacs-reveal]]
  - You can find the corresponding source code [[https://gitlab.com/schaepermeier/reveal-quiz-demo/][here]]
- First, set up the plugin to be included as with plain reveal.js
- It is recommended that you define the quiz-script in a different file and link to it with ~src="/path/to/quiz.js"~
  - org-reveal only supports single-line raw HTML

#+BEGIN_SRC org
#+REVEAL_HTML: <script data-quiz src="./quizzes/dummy-quiz.js"></script>
#+END_SRC

#+MACRO: copyrightyears 2018
#+INCLUDE: emacs-reveal/license-template.org

# Local Variables:
# indent-tabs-mode: nil
# End:
