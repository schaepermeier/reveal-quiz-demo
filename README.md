# reveal.js-quiz demo

This repository hosts some reveal.js presentations demonstrating the capabilities of the reveal.js-quiz plugin. Mainly it features a [demo](http://schaepermeier.gitlab.io/reveal-quiz-demo/demo.html) on GitLab Pages.

This project was created from [emacs-reveal-howto](https://gitlab.com/oer/emacs-reveal-howto) and uses [emacs-reveal](https://gitlab.com/oer/emacs-reveal) to generate [reveal.js presentations](https://revealjs.com/) from text files in [Org Mode](http://orgmode.org/).
